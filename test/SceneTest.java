/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author victoria.anderson
 */
public class SceneTest {
    
    public SceneTest() {
    }
    @Test
    public void testGetHeight() {
        Scene scene = new Scene(0, 0);
        assertEquals(0, scene.getHeight ());
        Scene scene2 = new Scene(10, 5);
        assertEquals(10, scene2.getHeight ());
    }

     @Test
    public void testGetWidth() {
        Scene scene = new Scene(0, 0);
        assertEquals(0, scene.getWidth ());
        Scene scene2 = new Scene(10, 5);
        assertEquals(5, scene2.getWidth ());
    }
    
    @Test
    public void testAddTerrain(){
        Scene scene = new Scene (5,3);
        scene.addTerrain ("G",2,1);
        scene.addTerrain("T", 4,2);
        assertEquals ("G", scene.getTerrain (2,1));
        assertEquals ("T", scene.getTerrain(4,2));
    }
}
