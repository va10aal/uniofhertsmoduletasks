/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author victoria.anderson
 */

public class Scene {
   int height;
   int width;
   private String [][] terrain;

   public Scene (int height, int width){
    this.height= height;
    this.width = width;
     terrain = new String [height][width]; 
    }

    public int getHeight () {
        return height;
    }

public int getWidth(){
    return width;
}

public void addTerrain (String type, int row, int column){
  terrain [row][column]= type;
}
public String getTerrain (int row, int column){
    return terrain [row][column];
} 
}
